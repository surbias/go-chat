# go chat
### Sample chat application built with the GO programming language.

#### Description

Set the host and guest of the application using the command line tools.

#### Usage:
[compiled]
Go to the bin folder and run the file for mac / windows:

```
# mac
# setup the host

> ./go-chat -listen 127.0.0.1

# on another terminal window
> ./go-chat 127.0.0.1

# windows

# setup the host

> go-chat.exe -listen 127.0.0.1

# on another terminal window
> go-chat.exe 127.0.0.1

```

[debugging]
Go the source folder and run the application using the go cli

```
# setup the host

> go run main.go -listen 127.0.0.1

# on another terminal window
> go run main.go 127.0.0.1
```


#### simple setup:
- download the [go binary release](https://storage.googleapis.com/golang/go1.7.1.darwin-amd64.pkg) and install it.
- setup the environment variables like so (mac-only example setup):

```bash
# path to instalation folder
export GOROOT=/usr/local/go
# path to base project workspace
export GOPATH=$HOME/development/go-chat
# append the go cli to the path variable	
export PATH=$PATH:$GOROOT/bin
```